import os
import time
import tensorflow as tf
from tensorflow.contrib import tpu
from tensorflow.contrib.cluster_resolver import TPUClusterResolver

def axy_computation(x):
  #x = tf.matmul(x,y)
  return tf.cholesky(x)

n=1024

inputs = [
    tf.eye(n, dtype=tf.float32),
    #tf.ones([n, n], dtype=tf.bfloat16),
]

tpu_computation = tpu.rewrite(axy_computation, inputs)

tpu_grpc_url = TPUClusterResolver(
    tpu=[os.environ['TPU_NAME']]).get_master()

with tf.Session(tpu_grpc_url) as sess:
  sess.run(tpu.initialize_system())
  sess.run(tf.global_variables_initializer())
  start = time.time()
  output = sess.run(tpu_computation)
  stop = time.time()
  print(output)
  print(stop-start)
  sess.run(tpu.shutdown_system())

print('Done!')
